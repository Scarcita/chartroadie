import React, {createContext, useContext, useMemo, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Home from './index'
import Login from "./login";
  
  const AuthStateContext = createContext();
  
  export default function useAuth() {
    return useContext(AuthStateContext);
  }
  
  export function AuthProvider({ children }) {
    const [user] = useState();
    const [ isLoading, setisLoading] = useState(true)
    const router = useRouter();
    let role = user?.role;
    let allowed = true;
  
    if (router.pathname.startsWith("/plans") && role !== "ADMIN") {
      allowed = false;
    }
    if (router.pathname.startsWith("/dashboard") && role !== "ADMIN") {
      allowed = false;
    }
    if (router.pathname.startsWith("/calendar") && role !== "ADMIN") {
      allowed = false;
    }
    if (router.pathname.startsWith("/clients") && role !== "ADMIN") {
      allowed = false;
    }
    if (router.pathname.startsWith("/board") && role !== "ADMIN") {
      allowed = false;
    }
    if (router.pathname.startsWith("/ads") && role !== "ADMIN") {
    allowed = false;
    }
    if (router.pathname.startsWith("/board") && role !== "designer") {
        allowed = false;
    }
    if (router.pathname.startsWith("/calendar") && role !== "manager") {
        allowed = false;
    }
    if (router.pathname.startsWith("/clients") && role !== "manager") {
    allowed = false;
    }
    if (router.pathname.startsWith("/calendar") && role !== "community") {
    allowed = false;
    }
    if (router.pathname.startsWith("/dashboard") && role !== "accountant") {
    allowed = false;
    }
    if (router.pathname.startsWith("/clients") && role !== "accountant") {
    allowed = false;
    }
    if (router.pathname.startsWith("/ads") && role !== "accountant") {
    allowed = false;
    }
  
    useEffect(() => {
      role = user?.role;
      //console.log('ROLE DEFINIDO: ', role);
      if (!isLoading) {
        // if (router.pathname.startsWith("/USER") && role !== "USER") {
        //   if (role) {
        //     console.log(router.pathname.replace("USER", role));
        //     router.push(router.pathname.replace("USER", role));
        //   } else {
        //     // router.push("/");
        //   }
        // }
        if (router.pathname.startsWith("/ADMIN") && role !== "ADMIN") {
          if (role) {
            //console.log(router.pathname.replace("ADMIN", role));
            router.push(router.pathname.replace("ADMIN", role));
          } else {
            router.push("/board");
          }
        }
      }
    }, [user, router]);
  
    const contextValue = useMemo(() => [allowed, role], [allowed, role]);
    const ComponentToRender = allowed ? children : Home;
  
    return (
      <AuthStateContext.Provider value={contextValue}>
        {ComponentToRender}
      </AuthStateContext.Provider>
    );
  }
  