import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import Image from 'next/image';
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";


import ImgFacebook from '../../../public/Plans/facebook.svg'
import ImgGmail from '../../../public/Plans/gmail.svg'
import ImgIg from '../../../public/Plans/instagram.svg'
import ImgMessenger from '../../../public/Plans/messenger.svg'
import ImgTelegram from '../../../public/Plans/telegram.svg'
import ImgTiktok from '../../../public/Plans/tikTok.svg'
import ImgTwitter from '../../../public/Plans/twitter.svg'
import ImgWhatsapp from '../../../public/Plans/whatsapp.svg'
import ImgYoutube from '../../../public/Plans/youtube.svg'



export default function ModalEditPlansAll(props) {
    const {
        row,
        id, 
        name,
        facebook_posts_qty,
        instagram_posts_qty,
        mailing_posts_qty,
        youtube_posts_qty,
        tiktok_posts_qty,
        linkedin_posts_qty,
        twitter_posts_qty,
        has_included_ads,
        included_ads_qty,
        price,
        reloadPlansAll,
        setReloadPlansAll
        } = props;
        console.log('Name: ' + id);
        const [isLoading, setIsLoading] = useState(false)
        const [showDeleteModal, setShowDeleteModal] = useState(false);
        const [showEditModal, setShowEditModal] = useState(false);
        const [nameForm, setNameForm] = useState(name)
        const [facebookForm, setFacebookForm] = useState('')
        const [facebookPostsGtyForm, setFacebookPostsGtyForm] = useState(facebook_posts_qty)
        const [instagramForm, setInstagramForm] = useState('')
        const [instagramPostsGtyForm, setInstagramPostsGtyForm] = useState(instagram_posts_qty)
        const [mailingForm, setMailingForm] = useState('')
        const [mailingPostsGtyForm, setMailingPostsGtyForm] = useState(mailing_posts_qty)
        const [youtubeForm, setYoutubeForm] = useState('')
        const [youtubePostsGtyForm, setYoutubePostsGtyForm] = useState(youtube_posts_qty)
        const [tiktokForm, setTiktokForm] = useState('')
        const [tiktokPostsGtyForm, setTiktokPostsGtyForm] = useState(tiktok_posts_qty)
        const [linkedinForm, setLinkedinForm] = useState('')
        const [linkedinPostsGtyForm, setLinkedinPostsGtyForm] = useState(linkedin_posts_qty)
        const [twitterForm, setTwitterForm] = useState('')
        const [twitterPostsGtyForm, setTwitterPostsGtyForm] = useState(twitter_posts_qty)
        const [hasIncludedAdsForm, setHasIncludedAdsForm] = useState(has_included_ads)
        const [includedAdsQtyForm, setIncludedAdsQtyForm] = useState(included_ads_qty)
        const [priceForm, setPriceForm] = useState(price)


    const actualizarDatos = async (formData) => {

        setIsLoading(true)
        var data = new FormData();

        
        data.append("name", nameForm);
        data.append("facebook", facebookForm ? '1' : '0');
        data.append("facebook_posts_qty", facebookPostsGtyForm);
        data.append("instagram", instagramForm ? '1' : '0');
        data.append("instagram_posts_qty", instagramPostsGtyForm);
        data.append("mailing", mailingForm ? '1' : '0');
        data.append("mailing_posts_qty", mailingPostsGtyForm);
        data.append("youtube", youtubeForm ? '1' : '0');
        data.append("youtube_posts_qt", youtubePostsGtyForm);
        data.append("tiktok", tiktokForm ? '1' : '0');
        data.append("tiktok_posts_qty", tiktokPostsGtyForm);
        data.append("linkedin", linkedinForm ? '1' : '0');
        data.append("linkedin_posts_qty", linkedinPostsGtyForm);
        data.append("twitter", twitterForm ? '1' : '0');
        data.append("twitter_posts_qty", twitterPostsGtyForm);
        data.append("has_included_ads", hasIncludedAdsForm ? '1' : '0');
        data.append("included_ads_qty", includedAdsQtyForm);
        data.append("price", priceForm);

        fetch("https://slogan.com.bo/roadie/plans/editMobile/" + id, {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
            console.log('VALOR ENDPOINTS EDIT: ', data);
            setIsLoading(false)
            if (data.status) { 
                setReloadPlansAll(!reloadPlansAll)
                setShowEditModal(false)
                console.log('edit endpoint: ' + data.status);
            } else {
                console.error(data.error)
            }
        })
    
      }

    const validationSchema = Yup.object().shape({
    name: Yup.string()
    .required('name is required'),

    // facebook: Yup.string()
    //   .required("facebook is required"),
    // // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    // facebookPostsGty: Yup.string()
    //   .required('facebookPostsGty is required'),
    // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    // instagram: Yup.string()
    //   .required('instagram is required'),
    // // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    // instagramPostsGty: Yup.number()
    //   .required('instagramPostsGty is required'),
    // //   //.min(6, 'minimo 6 caracteres'),
    // // //.matches(/[a-zA-Z0-9]/, 'solo numeros y letras'),

    // mailing: Yup.string()
    //   .required('mailing is required'),

    // mailingPostsGty: Yup.string()
    //   .required('mailingPostsGty is required'),

    // youtube: Yup.string()
    //   .required('youtube is required'),

    // youtubePostsGty: Yup.string()
    // .required('youtubePostsGty is required'),
    // // //.matches(/^[\(]?[\+]?(\d{2}|\d{3})[\)]?[\s]?((\d{6}|\d{8})|(\d{3}[\*\.\-\s]){3}|(\d{2}[\*\.\-\s]){4}|(\d{4}[\*\.\-\s]){2})|\d{8}|\d{10}|\d{12}$/, 'Ingrese un numero valido'),

    // tiktok: Yup.string()
    // .required('tiktok is required'),

    // tiktokPostsGty: Yup.string()
    // .required('tiktokPostsGty is required'),
    // // //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    // linkedin: Yup.string()
    // .required('linkedin is required'),
    // // // //.matches(/[A-za-z0-9]/, 'solo se permite numeros'),

    // linkedinPostsGty: Yup.string()
    // .required('linkedinPostsGty is required'),
    // // //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    // twitter: Yup.string()
    // .required('twitter is required'),
    // // //.matches(/^[\(]?[\+]?(\d{2}|\d{3})[\)]?[\s]?((\d{6}|\d{8})|(\d{3}[\*\.\-\s]){3}|(\d{2}[\*\.\-\s]){4}|(\d{4}[\*\.\-\s]){2})|\d{8}|\d{10}|\d{12}$/, 'Ingrese un numero valido'),

    // twitterPostsGty: Yup.string()
    // .required('twitterPostsGty is required'),
    // // //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    hasIncludedAds: Yup.string()
        .required('hasIncludedAds is required'),
    //   //.matches(/^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/, 'Date must be a valid date in the format YYYY-MM-DD'),
    
    includedAdsQty: Yup.string()
    .required('includedAdsQty is required'),

    price: Yup.string()
    .required('price is required'),


    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        console.log('onSubmit data clients:');
        console.log(data);
        actualizarDatos(data);
    }

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)}>

                <div className="mt-[20px] grid grid-cols-12 gap-4">

                    <div className='col-span-12 md:col-span-12 lg:col-span-12 '>

                            <p className='text-[12px] text-[#C1C1C1] mr-[10px] text-start'>Name</p>
                            <input name="name" type="text" id="name" className={`w-full h-[30px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                            placeholder='Ingrese su nombre...'
                            {...register('name')}
                            value={nameForm}
                            onChange={(e) => {
                                setNameForm(e.target.value)
                            }}/>
                            <div className="text-[14px] text-[#FF0000]">{errors.name?.message}</div>

                    </div>


                </div>


                <div className="mt-[20px] grid grid-cols-12 gap-3">

                    <div className='col-span-6 md:col-span-6 lg:col-span-4 '>
                        <div className="flex flex-row">
                            <input name="facebook"   type="checkbox" id="" className="mr-[5px]" 
                            {...register('facebook')}
                            value={facebookForm}
                            onChange={(e) => {
                                setFacebookForm(e.target.value)
                            }}/>
                            <Image
                            src={ImgFacebook}
                            alt='ImgFacebook'
                            layout='fixed'
                            width={25}
                            height={25}
                        />
                        <input name="facebookPostsGty" type="number" {...register('facebookPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                        placeholder='Posts Qty'
                        value={facebookPostsGtyForm}
                        onChange={(e) => {
                            setFacebookPostsGtyForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.facebookPostsGty?.message}</div>
                            
                        </div>
                    </div>

                    <div className='col-span-6 md:col-span-6 lg:col-span-4 '>
                        <div className="flex flex-row">
                            <input name="instagram" type="checkbox" id="acceptTermsandConditions"  className="mr-[5px]" 
                            {...register('instagram')}
                            value={instagramForm}
                            onChange={(e) => {
                                setInstagramForm(e.target.value)
                            }}/>
                            <Image
                            src={ImgIg}
                            alt='ImgIg'
                            layout='fixed'
                            width={25}
                            height={25}
                        />
                        <input name="instagramPostsGty" type="number" {...register('instagramPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                        placeholder='Posts Qty'
                        value={instagramPostsGtyForm}
                        onChange={(e) => {
                            setInstagramPostsGtyForm(e.target.value)
                        }}
                        />

                        <div className="text-[14px] text-[#FF0000]">{errors.instagramPostsGty?.message}</div>
                            
                        </div>
                    </div>

                    <div className='col-span-6 md:col-span-6 lg:col-span-4 '>
                        <div className="flex flex-row">
                            <input name="mailing" type="checkbox" id="acceptTermsandConditions" className="mr-[5px]"
                            {...register('mailing')}
                            value={mailingForm}
                            onChange={(e) => {
                                setMailingForm(e.target.value)
                            }} />
                            <Image
                            src={ImgGmail}
                            alt='ImgGmail'
                            layout='fixed'
                            width={25}
                            height={25}
                        />
                        <input name="mailingPostsGty" type="number" {...register('mailingPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                        placeholder='Posts Qty'
                        value={mailingPostsGtyForm}
                        onChange={(e) => {
                            setMailingPostsGtyForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.mailingPostsGty?.message}</div>
                            
                        </div>
                    </div>
                    

                    <div className='col-span-6 md:col-span-6 lg:col-span-3 '>
                        <div className="flex flex-row">
                            <input name="youtube" type="checkbox" id="acceptTermsandConditions" className="mr-[5px]"
                            {...register('youtube')} 
                            value={youtubeForm}
                            onChange={(e) => {
                                setYoutubeForm(e.target.value)
                            }}/>
                            <Image
                            src={ImgYoutube}
                            alt='ImgYoutube'
                            layout='fixed'
                            width={25}
                            height={25}
                        />
                        <input name="youtubePostsGty" type="number" {...register('youtubePostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                        placeholder='Posts Qty'
                        value={youtubePostsGtyForm}
                        onChange={(e) => {
                            setYoutubePostsGtyForm(e.target.value)
                        }}
                        />

                        <div className="text-[14px] text-[#FF0000]">{errors.youtubePostsGty?.message}</div>
                            
                        </div>
                    </div>

                    <div className='col-span-6 md:col-span-6 lg:col-span-3 '>
                        <div className="flex flex-row">
                            <input name="tiktok" type="checkbox" id="acceptTermsandConditions" className="mr-[5px]"
                            {...register('tiktok')}
                            value={tiktokForm}
                            onChange={(e) => {
                                setTiktokForm(e.target.value)
                            }}/>
                            <Image
                            src={ImgTiktok}
                            alt='ImgTiktok'
                            layout='fixed'
                            width={25}
                            height={25}
                        />
                        <input name="tiktokPostsGty" type="number" {...register('tiktokPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                        placeholder='Posts Qty'
                        value={tiktokPostsGtyForm}
                        onChange={(e) => {
                            setTiktokPostsGtyForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.tiktokPostsGty?.message}</div>
                            
                        </div>
                    </div>

                    <div className='col-span-6 md:col-span-6 lg:col-span-3'>
                        <div className="flex flex-row">
                            <input name="linkedin" type="checkbox" id="acceptTermsandConditions" className="mr-[5px]"
                            {...register('linkedin')}
                            value={linkedinForm}
                            onChange={(e) => {
                                setLinkedinForm(e.target.value)
                            }}/>
                            <Image
                            src={ImgTelegram}
                            alt='ImgTelegram'
                            layout='fixed'
                            width={25}
                            height={25}
                        />
                        <input name="linkedinPostsGty" type="number" {...register('linkedinPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                        placeholder='Posts Qty'
                        value={linkedinPostsGtyForm}
                        onChange={(e) => {
                            setLinkedinPostsGtyForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.linkedinPostsGty?.message}</div>
                            
                        </div>
                    </div>

                    <div className='col-span-6 md:col-span-6 lg:col-span-3'>
                        <div className="flex flex-row">
                            <input name="twitter" type="checkbox" id="acceptTermsandConditions" {...register('twitter')} className="mr-[5px]"
                            {...register('twitter')}
                            value={twitterForm}
                            onChange={(e) => {
                                setTwitterForm(e.target.value)
                            }}/>
                            <Image
                            src={ImgTwitter}
                            alt='ImgTwitter'
                            layout='fixed'
                            width={25}
                            height={25}
                        />
                        <input name="twitterPostsGty" type="number" {...register('twitterPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                        placeholder='Posts Qty'
                        value={twitterPostsGtyForm}
                        onChange={(e) => {
                            setTwitterPostsGtyForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.twitterPostsGty?.message}</div>
                            
                        </div>
                    </div>
                    

                </div>

                <div className="mt-[15px] grid grid-cols-12 gap-3">

                    <div className='col-span-6 md:col-span-6 lg:col-span-6 '>
                        <div className="flex flex-row">
                            <input name="acceptTermsandConditions" type="checkbox" id="acceptTermsandConditions"
                            {...register('hasIncludedAds')}
                            value={hasIncludedAdsForm}
                            onChange={(e) => {
                                setHasIncludedAdsForm(e.target.value)
                            }}/>
                            <p className='text-[12px] text-[#C1C1C1] ml-[10px] '>Has included ads </p>
                            
                            
                        </div>
                        <input name="includedAdsQty" type="number" {...register('includedAdsQty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                            placeholder='Posts Qty'
                            value={includedAdsQtyForm}
                            onChange={(e) => {
                                setIncludedAdsQtyForm(e.target.value)
                            }}
                            />
                            <div className="text-[14px] text-[#FF0000]">{errors.includedAdsQty?.message}</div>
                    </div>

                    <div className='col-span-6 md:col-span-6 lg:col-span-6 '>
                        <p className='text-[12px] text-[#C1C1C1] mr-[10px] text-start'>Price</p>
                        <input name="price" type="text" {...register('price')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                        placeholder='Price'
                        value={priceForm}
                        onChange={(e) => {
                            setPriceForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.price?.message}</div>
                            
                    </div>
                    

                </div>


                <div className="flex flex-row justify-between mt-[20px]">

                    <div>
                        <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                    </div>

                    <div>
                        <button
                            className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                            onClick={() =>
                                setShowEditModal(false)
                            }
                            disabled={isLoading}
                        >
                            Cancel
                        </button>
                        <button
                            className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                            type="submit"
                            disabled={isLoading}
                        >
                            {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Update'}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );
}