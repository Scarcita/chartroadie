import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ReactSelect from 'react-select';



export default function ModalEditPlans(props) {
    const { 
        id,
        row,
        client_id,
        plan,
        startDate,
        endDate,
        reloadPlans ,
        setReloadPlans} = props;
        console.log('ID CLIENT:' + client_id);
    const [isLoading, setIsLoading] = useState(false)
    const [showEditModal, setShowEditModal] = useState(false);
    const [clientPlanForm, setClientPlanForm] = useState()
    const [planIdForm, setPlanIdForm] = useState(plan)
    const [startDateForm, setStartDateForm] = useState(startDate)
    const [endDateForm, setEndDateForm] = useState(endDate)


    const formatOptionLabel = ({ value, label, post }) => {

        return (
            <div className='grid grid-cols-12 align-center'>
                <div>
                        <small className='text-gray-500 text-xs'>{post.name}</small>
                </div>
            </div>
        )
    };      


    useEffect(() => {

        fetch('https://slogan.com.bo/roadie/plans/all')
        .then(response => response.json())
        .then(data => {
            if (data.status) {

                var temp = [];
                data.data.map((result) => {
                    temp.push({value: result.id, label: result.name, post: result})
                })

                setClientPlanForm(temp)
            } else {
                console.error(data.error)
            }
            setIsLoading(false)
        })

    }, [])


    const actualizarDatos = async (formData) => {

        setIsLoading(true)
        var data = new FormData();

        data.append("client_id", client_id );
        data.append("plan_id", planIdForm.value);
        data.append("start_date", startDateForm );
        data.append("end_date", endDateForm);
    
        fetch("http://slogan.com.bo/roadie/clientsPlans/editMobile/" + row.id , {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
            console.log('VALOR ENDPOINTS EDIT: ', data);
            setIsLoading(false)
            if (data.status) { 
                setReloadPlans(!reloadPlans)
                setShowEditModal(false)
                console.log('edit endpoint: ' + data.status);
            } else {
                console.error(data.error)
            }
        })
    
      }

    const validationSchema = Yup.object().shape({
        startDate: Yup.string()
            .required('Is required'),
            //.matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        endDate: Yup.string()
            .required('Is required'),
            //.matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        console.log('onSubmit data:');
        console.log(data);
        actualizarDatos(data);
    }

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)} >

            <div className="mt-[20px] grid grid-cols-12 gap-4">
                <div className='col-span-12 md:col-span-12 lg:col-span-12 text-left'>
                    <p className='text-[12px] text-[#C1C1C1] mr-[10px] text-left'>Select Plan</p>
                    <ReactSelect 
                        defaultValue={planIdForm}
                        onChange={setPlanIdForm}
                        formatOptionLabel={formatOptionLabel}
                        options={clientPlanForm}
                        />

                </div>

                <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                    <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Select Date start </p>
                    <input name="startDate" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                    {...register('startDate')}
                    value={startDateForm}
                    onChange={(e) => {
                        setStartDateForm(e.target.value)
                    }}
                    />
                    <div className="text-[14px] text-[#FF0000]">{errors.startDate?.message}</div>
                </div>

                <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                    <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Select Date End </p>
                    <input name="endDate" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                    {...register('endDate')}
                    value={endDateForm}
                    onChange={(e) => {
                        setEndDateForm(e.target.value)
                    }}
                    />
                    <div className="text-[14px] text-[#FF0000]">{errors.endDate?.message}</div>
                </div>


                </div>

                <div className="flex flex-row justify-between mt-[20px]">

                    <div>
                        <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                    </div>

                    <div>
                        <button
                            className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                            onClick={() =>
                                setShowEditModal(false)
                            }
                            disabled={isLoading}
                        >
                            Cancel
                        </button>
                        <button
                            className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                            type="submit"
                            disabled={isLoading}
                        >
                            {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Update'}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );
}