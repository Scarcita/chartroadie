import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/router'
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";



export default function ModalExtras(props) {


    const { reloadExtras, setReloadExtras, client_plan_id} = props;
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    //const [type, setType] = useState('')
    const [detail, setDetail] = useState('')
    const [price, setPrice] = useState('')
    //const [media_url, setMediaUrl] = useState('')

    const clearForm = () => {
        //setType('')
        setDetail('')
        setPrice('')
        //setMediaUrl('')
    }
   
    const registerExtraPost = async (formData) => {

        setIsLoading(true)
        var data = new FormData();

        //console.log(formData.mediaUrl);
    
        data.append("client_plan_id", client_plan_id);
        data.append("type", formData.type);
        data.append("detail", detail);
        data.append("price", price);
        //data.append("media_url", media_url);
        
        if(formData.mediaUrl.length !== 0){
            data.append("media_url", formData.mediaUrl[0]);
        }
        // data.append("media_url", mediaUrl);
    
        fetch("http://slogan.com.bo/roadie/clientsPlansExtras/addMobile", {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
              console.log('VALOR ENDPOINTS: ', data);
              setIsLoading(false)
              if(data.status){
                clearForm()
                setReloadExtras(!reloadExtras)
                setShowModal(false)
                //IMPLEMENTAR RECARGA DE DATOS (COMPARTIR FUNCION DE PANTALLA AL MODAL)
              } else {
                alert(JSON.stringify(data.errors, null, 4))
              }
              
            },
            (error) => {
              console.log(error)
            }
          )
    
      }
    
    
    const validationSchema = Yup.object().shape({
        type: Yup.string()
        .required('type is required'),

        detail: Yup.string()
            .required("detail is required"),
        // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        // mediaUrl: Yup.string()
        //     .required('mediaUrl is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        price: Yup.string()
            .required('price is required'),
    
        });
        const formOptions = { resolver: yupResolver(validationSchema) };
    
        // get functions to build form with useForm() hook
        const { register, handleSubmit, reset, formState } = useForm(formOptions);
        const { errors } = formState;
    
        function onSubmit(data) {
            alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
            console.log('string is NOT empty')
            registerExtraPost(data)
            return false;
            
        }
    

    



    return (
        <>
            <div>
                <button
                    className="w-[112px] h-[26px] md:w-[200px] md:h-[48px] lg:w-[160px] lg:h-[35px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[14px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7] justify-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Create Extras
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        CREATE EXTRAS
                                    </h4>

                                </div>

                                <div>
                                    {/* {client_plan_id } */}
                                    <form onSubmit={handleSubmit(onSubmit)} >

                                        <div className="mt-[20px] grid grid-cols-12 gap-4">

                                            <div className='col-span-6 md:col-span-6 lg:col-span-6'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Type</p>
                                                <select
                                                    {...register('type')}
                                                    name="type"
                                                    className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                >
                                                    <option value="Image">Image</option>
                                                    <option value="Album">Album</option>
                                                    <option value="Video">Video</option>
                                                    <option value="Story">Story</option>
                                                    <option value="Reel">Reel</option>
                                                </select>
                                                <div className="text-[14px] text-[#FF0000]">{errors.type?.message}</div>
                                            </div>
                                            <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Detail</p>
                                                <input name="detail"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                                                {...register('detail')}
                                                value={detail}
                                                onChange={(e) => {
                                                    setDetail(e.target.value)
                                                }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.detail?.message}</div>
                                            </div>
        
                                            <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Price</p>
                                                <input name='price' type="number" placeholder="1.0" step="0.01" min="0.01"  className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                                                {...register('price')}
                                                value={price}
                                                onChange={(e) => {
                                                    setPrice(e.target.value)
                                                }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.price?.message}</div>
                                            </div>
                                            <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Image</p>
                                                <input 
                                                    // name="mediaUrl"  
                                                    type={'file'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pt-[7px] text-[12px]'
                                                {...register('mediaUrl')}
                                                // value={mediaUrl}
                                                // onChange={(e) => {
                                                //     setMediaUrl(e.target.value)
                                                // }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.mediaUrl?.message}</div>
                                            </div>

                                        </div>

                                        <div className="flex flex-row justify-between mt-[20px]">

                                            <div>
                                                <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                            </div>

                                            <div>
                                                <button
                                                    className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                    onClick={() =>
                                                        setShowModal(false)
                                                    }
                                                    disabled={isLoading}
                                                >
                                                    Cancel
                                                </button>
                                                <button
                                                    className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                    type={'submit'}
                                                    disabled={isLoading}
                                                >
                                                    {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}