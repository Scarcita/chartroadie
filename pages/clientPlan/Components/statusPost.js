import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'

 /////// IMAGENES ////////////////////
 import ImgFacebook from '../../../public/Dashhoard/facebook.svg'
 import ImgGmail from '../../../public/Dashhoard/gmail.svg'
 import ImgIg from '../../../public/Dashhoard/instagram.svg'
 import ImgMessenger from '../../../public/Dashhoard/messenger.svg'
 import ImgTelegram from '../../../public/Dashhoard/telegram.svg'
 import ImgTiktok from '../../../public/Dashhoard/tikTok.svg'
 import ImgTwitter from '../../../public/Dashhoard/twitter.svg'
 import ImgWhatsapp from '../../../public/Dashhoard/whatsapp.svg'
 import ImgYoutube from '../../../public/Dashhoard/youtube.svg'

const StatusPost = (props) => {
    const { client_plan_id } = props;

    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState([]);
    const [counterPosted, setCounterPosted] = useState(0);

    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clientsPlansPosts/all/' + client_plan_id )
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log('STATUSPOST: ' + data.data);
                    setData(data.data)
                    
                    let temporal = 0;
                    for (let i = 0; i < data.length; i++) {
                        data[i].social_network == "Facebook" ?
                            (
                                temporal++,
                                setCounterPosted(temporal)
                            )
                            :
                            null
                    }
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

    }, [])

    // useEffect(() => {
    //     fetch('https://slogan.com.bo/roadie/clientsPlans/all/')
    //         .then(response => response.json())
    //         .then(data => {
    //             if (data.status) {
    //                 setData(data.data)
    //                 console.log(data.data);
    //             } else {
    //                 console.error(data.error)
    //             }
    //             setIsLoading(false)
    //         })

    // }, [])


    return (
        isLoading ?
        <>
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>

        </>
        :
        <div className="mt-[20px] grid grid-cols-12 gap-3">
            <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#0062E0] to-[#19AFFF] rounded-[16px] justify-between p-[8px]'>
                <div>
                    <div className="flex flex-row">
                        <p className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">{counterPosted} / 112</p>
                            
                    </div>
                <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[10px]'>
                    <Image
                        src={ImgFacebook}
                        layout='fixed'
                        alt='ImgFacebook'
                        width={36}
                        height={36}
                        
                    />
                
                </div>
            </div>
            <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#FCA759] via-[#E82D56] via-[#A22DB4] to-[#643DCE] rounded-[16px] justify-between pl-[10px] p-[8px]'>
                <div>
                    <div className="flex flex-row">
                        <p className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">/112</p>
                            
                    </div>
                <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[10px]'>
                    <Image
                        src={ImgIg}
                        layout='fixed'
                        alt='ImgIg'
                        width={36}
                        height={36}
                        
                    />
                
                </div>
            </div>
            <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row  bg-[#161616] rounded-[16px] justify-between p-[8px]'>
                <div>
                    <div className="flex flex-row">
                        <p className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">/112</p>
                            
                    </div>
                <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[10px]'>
                    <Image
                        src={ImgTiktok}
                        layout='fixed'
                        alt='ImgTiktok'
                        width={36}
                        height={36}
                        
                    />
                </div>
            </div>
            <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#0CA8F6] to-[#0096E1] rounded-[16px] justify-between p-[8px]'>
                <div>
                    <div className="flex flex-row">
                        <p className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">/112</p>
                            
                    </div>
                <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[10px]'>
                    <Image
                        src={ImgTelegram}
                        layout='fixed'
                        alt='ImgTelegram'
                        width={36}
                        height={36}
                        
                    />
                </div>
            </div>
            <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#DB0505] to-[#FF0000] rounded-[16px] justify-between p-[8px]'>
                <div>
                    <div className="flex flex-row">
                        <p className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">/112</p>
                            
                    </div>
                <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[10px]'>
                    <Image
                        src={ImgYoutube}
                        layout='fixed'
                        alt='ImgYoutube'
                        width={36}
                        height={36}
                        
                    />
                </div>
            </div>
            <div className='col-span-6 md:col-span-6 lg:col-span-2 flex flex-row bg-gradient-to-l from-[#FBBC04] to-[#FCD462] rounded-[16px] justify-between p-[8px]'>
                <div>
                    <div className="flex flex-row">
                        <p className="text-[28px] md:text-[30px] lg:text-[32px] font-semibold text-[#FFFFFF]">/112</p>
                            
                    </div>
                <div className="text-[10px] text-[#FFFFFF]">POST</div>
                </div>
                <div className='pt-[10px]'>
                    <Image
                        src={ImgGmail}
                        layout='fixed'
                        alt='ImgGmail'
                        width={36}
                        height={36}
                        
                    />
                </div>
            </div>
        </div>   
 
    )
};






export default StatusPost;