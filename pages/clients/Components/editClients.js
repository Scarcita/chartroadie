import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";



export default function ModalEditClients(props) {
    const { row, 
        name, 
        country, 
        city, 
        email, 
        website, 
        phone,
        client_since, 
        facebook_url, 
        instagram_url, 
        youtube_url, 
        tiktok_url, 
        twitter_url, 
        linkedin_url,
        reloadClients,  
        setReloadClients} = props;
    const [isLoading, setIsLoading] = useState(false)
    const [showEditModal, setShowEditModal] = useState(false);
    const [nameForm, setNameForm] = useState(name)
    const [countryForm, setCountryForm] = useState(country)
    const [cityForm, setCityForm] = useState(city)
    const [emailForm, setEmailForm] = useState(email)
    const [websiteForm, setWebsiteForm] = useState(website)
    const [phoneForm, setPhoneForm] = useState(phone)
    const [clientSinceForm, setClientSinceForm] = useState(client_since)
    const [facebookUrlForm, setFacebookUrlForm] = useState(facebook_url)
    const [instagramUrlForm, setInstagramUrlForm] = useState(instagram_url)
    const [youtubeUrlForm, setYoutubeUrlForm] = useState(youtube_url)
    const [tiktokUrlForm, setTiktokUrlForm] = useState(tiktok_url)
    const [twitterUrlForm, setTwitterUrlForm] = useState(twitter_url)
    const [linkedinUrlForm, setLinkedinUrlForm] = useState(linkedin_url)


    const actualizarDatos = async (formData) => {

        setIsLoading(true)
        var data = new FormData();

        data.append("name", nameForm);
        data.append("country", countryForm);
        data.append("city", cityForm);
        data.append("email", emailForm);
        data.append("website", websiteForm);
        data.append("phone", phoneForm);
        data.append("client_since", clientSinceForm);
        data.append("facebook_url", facebookUrlForm);
        data.append("instagram_url", instagramUrlForm);
        data.append("youtube_url", youtubeUrlForm);
        data.append("tiktok_url", tiktokUrlForm);
        data.append("twitter_url", twitterUrlForm);
        data.append("linkedin_url", linkedinUrlForm);
    
        if(formData.imgUrl.length !== 0){
            data.append("img_url", formData.imgUrl[0]);
        }
        fetch("https://slogan.com.bo/roadie/clients/editMobile/" + row.id , {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
            console.log('VALOR ENDPOINTS EDIT: ', data);
            setIsLoading(false)
            if (data.status) { 
                setReloadClients(!reloadClients)
                setShowEditModal(false)
                console.log('edit endpoint: ' + data.status);
            } else {
                console.error(data.error)
            }
        })
    
      }

    const validationSchema = Yup.object().shape({
        name: Yup.string()
        .required('name is required'),

        country: Yup.string()
        .required("country is required"),
        // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        city: Yup.string()
        .required('city is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        email: Yup.string()
        .required('email is required'),
        //   //.min(6, 'minimo 6 caracteres'),
        // //.matches(/[a-zA-Z0-9]/, 'solo numeros y letras'),

        phone: Yup.string()
        .required('phone is required'),

        clientSince: Yup.string()
        .required('clientSince is required'),


        });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        console.log('onSubmit data clients:');
        console.log(data);
        actualizarDatos(data);
    }

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)}>

                <div className="mt-[20px] grid grid-cols-12 gap-4">

                    <div className='col-span-12 md:col-span-12 lg:col-span-6 '>
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>name</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]"
                            type="text"
                            name="name"
                            {...register('name')}
                            value={nameForm}
                            onChange={(e) => {
                                setNameForm(e.target.value)
                            }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.name?.message}</div>
                    </div>

                    <div className="col-span-12 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Image</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis pt-[7px] text-[12px]"
                            type="file"
                            //name="imgUrl"
                            {...register('imgUrl')}
                            // value={imgUrl}
                            // onChange={(e) => setimgUrl(e.target.value)}
                        />
                        {/* <div className="text-[14px] text-[#FF0000]">{errors.imgUrl?.message}</div> */}
                    </div>

                    <div className='col-span-12 md:col-span-12 lg:col-span-6'>
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Country</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]"
                            type="text"
                            name="country"
                            {...register('country')}
                            value={countryForm}
                            onChange={(e) => setCountryForm(e.target.value)}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.country?.message}</div>
                    </div>


                    <div className='col-span-12 md:col-span-6 lg:col-span-6'>
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>City</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px] mr-[10px]"
                            type="text"
                            name="city"
                            {...register('city')}
                            value={cityForm}
                            onChange={(e) => setCityForm(e.target.value)}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.city?.message}</div>
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Email</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]"
                            type="email"
                            name="email"
                            {...register('email')}
                            value={emailForm}
                            onChange={(e) => setEmailForm(e.target.value)}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.email?.message}</div>
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Website</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]"
                            type="text"
                            name="website"
                            {...register('website')}
                            value={websiteForm}
                            onChange={(e) => setWebsiteForm(e.target.value)}
                        />
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Phone</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]"
                            type="text"
                            name="phone"
                            {...register('phone')}
                            value={phoneForm}
                            onChange={(e) => setPhoneForm(e.target.value)}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.phone?.message}</div>
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Client Since</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]"
                            type="date"
                            name="clientSince"
                            {...register('clientSince')}
                            value={clientSinceForm}
                            onChange={(e) => setClientSinceForm(e.target.value)}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.clientSince?.message}</div>
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-6 ">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Facebbok</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]"
                            type="text"
                            name="facebookUrl"
                            {...register('facebookUrl')}
                            value={facebookUrlForm}
                            onChange={(e) => setFacebookUrlForm(e.target.value)}
                        />
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-6 ">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Instagram</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]"
                            type="text"
                            name="instagramUrl"
                            {...register('instagramUrl')}
                            value={instagramUrlForm}
                            onChange={(e) => setInstagramUrlForm(e.target.value)}
                        />
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-6 ">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>YouTube</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]"
                            type="text"
                            name="youtubeUrl"
                            {...register('youtubeUrl')}
                            value={youtubeUrlForm}
                            onChange={(e) => setYoutubeUrlForm(e.target.value)}
                        />
                    </div>
                    
                    <div className="col-span-12 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>TikTok</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]"
                            type="text"
                            name="tiktokUrl"
                            {...register('tiktokUrl')}
                            value={tiktokUrlForm}
                            onChange={(e) => setTiktokUrlForm(e.target.value)}
                        />
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Twitter</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]"
                            type="text"
                            name="twitterUrl"
                            {...register('twitterUrl')}
                            value={twitterUrlForm}
                            onChange={(e) => setTwitterUrlForm(e.target.value)}
                        />
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-6 ">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>LinkedIn</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]"
                            type="text"
                            name="linkedinUrl"
                            {...register('linkedinUrl')}
                            value={linkedinUrlForm}
                            onChange={(e) => setLinkedinUrlForm(e.target.value)}
                        />
                    </div>
                </div>
                <div className="flex flex-row justify-between mt-[20px]">

                    <div>
                        <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                    </div>

                    <div>
                        <button
                            className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                            onClick={() =>
                                setShowEditModal(false)
                            }
                            disabled={isLoading}
                        >
                            Cancel
                        </button>
                        <button
                            className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                            type="submit"
                            disabled={isLoading}
                        >
                            {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Update'}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );
}