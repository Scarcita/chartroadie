import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';


export default function CounterPlansMonth() {
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([ ]);
    const [weekPostedPosts, setWeekPostedPosts] = useState(0);


    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clients/all')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    var temp = [];
                    data.data.map((result) => {
                        temp.push(result)
                    })

                    const now = new Date();

                    const firstDay = new Date(now.getFullYear(), now.getMonth(), 1);
                    console.log(firstDay);
                    const lastDay = new Date(now.getFullYear(), now.getMonth() + 1, 0);
                    console.log(lastDay);

                    var weekPostedPostsCounter = 0; 

                        temp.map((post) => {
                        var clienteSince = new Date(post.client_since);

                        if(post.client_since !== null ){
                            if(clienteSince >= firstDay && clienteSince <= lastDay){
                                weekPostedPostsCounter++;
                                console.error(post);
                            }
                        }
                    })
                    setWeekPostedPosts(weekPostedPostsCounter);
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [])

    return (

        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#582BE7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div>{weekPostedPosts}</div>
    )
}