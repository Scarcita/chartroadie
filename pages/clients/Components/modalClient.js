import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Dots } from 'react-activity';
import "react-activity/dist/Dots.css";


export default function ModalClient(props) {


    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false)


    
    const [dataAdd, setAddData] = useState('')
    const [name, setName] = useState('')
    const [country, setCountry] = useState('')
    const [city, setCity] = useState('')
    const [email, setEmail] = useState('')
    const [website, setWebsite] = useState('')
    const [phone, setPhone] = useState('')
    const [clientSince, setClientSince] = useState('')
    const [facebookUrl, setFacebookUrl] = useState('')
    const [instagramUrl, setInstagramUrl] = useState('')
    const [youtubeUrl, setYoutubeUrl] = useState('')
    const [tiktokUrl, setTiktokUrl] = useState('')
    const [twitterUrl, setTwitterUrl] = useState('')
    const [linkedinUrl, setLinkedinUrl] = useState('')

    
    const clearForm = () => {
        setName('')
        setCity('')
        setCountry('')
        setEmail('')
        setWebsite('')
        setPhone('')
        setClientSince('')
        setFacebookUrl('')
        setInstagramUrl('')
        setYoutubeUrl('')
        setTiktokUrl('')
        setTwitterUrl('')
        setLinkedinUrl('')
        setYoutubeUrl('')
        setTiktokUrl('')
    }
    
  const getDataOrden = async (formData) => {
    
    setIsLoading(true)
        var data = new FormData();

         //console.log(formData.imgUrl);

    data.append("name", name);
    data.append("country", country);
    data.append("city", city);
    data.append("email", email);
    data.append("website", website);
    data.append("phone", phone);
    data.append("client_since", clientSince);
    data.append("facebook_url", facebookUrl);
    data.append("instagram_url", instagramUrl);
    data.append("youtube_url", youtubeUrl);
    data.append("tiktok_url", tiktokUrl);
    data.append("twitter_url", twitterUrl);
    data.append("linkedin_url", linkedinUrl);

    if(formData.imgUrl.length !== 0){
        data.append("img_url", formData.imgUrl[0]);
    }

    fetch("https://slogan.com.bo/roadie/clients/addMobile", {
      method: 'POST',
      body: data,
    })
      .then(response => response.json())
      .then(data => {
        //console.log('VALOR ENDPOINTS: ', data);

        setIsLoading(false)
        if(data.status){
          clearForm()
          //setReloadClient(!reloadClient)
          setShowModal(false)
        } else {
          alert(JSON.stringify(data.errors, null, 4))
        }
        
      },
      (error) => {
        console.log(error)
      }
      )

  }

  useEffect(() => {
    console.warn('ahora name: '+ name);
  }, [ name])
 


  ////////////////// VALIDATION ///////////////////

  
  const validationSchema = Yup.object().shape({
    name: Yup.string()
    .required('name is required'),

    country: Yup.string()
      .required("country is required"),
    // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    city: Yup.string()
      .required('city is required'),
    //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    email: Yup.string()
      .required('email is required'),
    //   //.min(6, 'minimo 6 caracteres'),
    // //.matches(/[a-zA-Z0-9]/, 'solo numeros y letras'),

    phone: Yup.string()
    .required('phone is required'),

    clientSince: Yup.string()
      .required('clientSince is required'),

    // imgUrl: Yup.string()
    // .required('imgUrl is required'),
    //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),


    });
    const formOptions = { resolver: yupResolver(validationSchema) };

    // get functions to build form with useForm() hook
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
        console.log('string is NOT empty')
        getDataOrden(data)
        return false;
        
    }

    



    return (
        <>
            <div>
                <button
                    className="w-[112px] h-[24px] md:w-[200px] md:h-[48px] lg:w-[240px] lg:h-[46px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[12px] md:text-[15px] lg:text-[15px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7] justify-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Create Client
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        New Client
                                    </h4>

                                </div>
                                <div>
                                    <form onSubmit={handleSubmit(onSubmit)}>

                                        <div className="mt-[20px] grid grid-cols-12 gap-4">

                                            <div className='col-span-12 md:col-span-12 lg:col-span-6 '>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>name</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="name"
                                                    {...register('name')}
                                                    value={name}
                                                    onChange={(e) => {
                                                        setName(e.target.value)
                                                    }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.name?.message}</div>
                                            </div>

                                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Image</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] pt-[7px] text-[12px]"
                                                    type="file"
                                                    //name="imgUrl"
                                                    {...register('imgUrl')}
                                                    // value={imgUrl}
                                                    // onChange={(e) => setimgUrl(e.target.value)}
                                                />
                                                {/* <div className="text-[14px] text-[#FF0000]">{errors.imgUrl?.message}</div> */}
                                            </div>

                                            <div className='col-span-12 md:col-span-12 lg:col-span-6'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Country</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="country"
                                                    {...register('country')}
                                                    value={country}
                                                    onChange={(e) => setCountry(e.target.value)}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.country?.message}</div>
                                            </div>


                                            <div className='col-span-12 md:col-span-6 lg:col-span-6'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>City</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px]"
                                                    type="text"
                                                    name="city"
                                                    {...register('city')}
                                                    value={city}
                                                    onChange={(e) => setCity(e.target.value)}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.city?.message}</div>
                                            </div>
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Email</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="email"
                                                    name="email"
                                                    {...register('email')}
                                                    value={email}
                                                    onChange={(e) => setEmail(e.target.value)}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.email?.message}</div>
                                            </div>
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Website</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="website"
                                                    {...register('website')}
                                                    value={website}
                                                    onChange={(e) => setWebsite(e.target.value)}
                                                />
                                            </div>
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Phone</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="phone"
                                                    {...register('phone')}
                                                    value={phone}
                                                    onChange={(e) => setPhone(e.target.value)}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.phone?.message}</div>
                                            </div>
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Client Since</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="date"
                                                    name="clientSince"
                                                    {...register('clientSince')}
                                                    value={clientSince}
                                                    onChange={(e) => setClientSince(e.target.value)}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.clientSince?.message}</div>
                                            </div>
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6 ">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Facebbok</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="facebookUrl"
                                                    {...register('facebookUrl')}
                                                    value={facebookUrl}
                                                    onChange={(e) => setFacebookUrl(e.target.value)}
                                                />
                                            </div>
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6 ">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Instagram</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="instagramUrl"
                                                    {...register('instagramUrl')}
                                                    value={instagramUrl}
                                                    onChange={(e) => setInstagramUrl(e.target.value)}
                                                />
                                            </div>
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6 ">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>YouTube</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="youtubeUrl"
                                                    {...register('youtubeUrl')}
                                                    value={youtubeUrl}
                                                    onChange={(e) => setYoutubeUrl(e.target.value)}
                                                />
                                            </div>
                                            
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>TikTok</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="tiktokUrl"
                                                    {...register('tiktokUrl')}
                                                    value={tiktokUrl}
                                                    onChange={(e) => setTiktokUrl(e.target.value)}
                                                />
                                            </div>
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Twitter</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="twitterUrl"
                                                    {...register('twitterUrl')}
                                                    value={twitterUrl}
                                                    onChange={(e) => setTwitterUrl(e.target.value)}
                                                />
                                            </div>
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6 ">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>LinkedIn</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="linkedinUrl"
                                                    {...register('linkedinUrl')}
                                                    value={linkedinUrl}
                                                    onChange={(e) => setLinkedinUrl(e.target.value)}
                                                />
                                            </div>
                                        </div>
                                        <div className="flex flex-row justify-between mt-[20px]">

                                            <div>
                                                <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                            </div>

                                            <div>
                                                <button
                                                    className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                    onClick={() =>
                                                        setShowModal(false)
                                                    }
                                                    disabled={isLoading}
                                                >
                                                    Cancel
                                                </button>
                                                <button
                                                    className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                   type="submit"
                                                   disabled={isLoading}

                                                >
                                                    {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                                    
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}