import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';


export default function CounterPostDay() {
    const [isLoading, setIsLoading] = useState(true);

    const [todayPosts, setTodayPosts] = useState(0);  

    useEffect(() => {

        fetch('https://slogan.com.bo/roadie/clientsPlansPosts/monthCalendar')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    let postsTemp = [];

                    console.log(data.data);

                    Object.values(data.data).map((date) => {
                        date.map((result) => {
                            postsTemp.push(result);
                        })
                    })

                    var todayFirstSec = new Date();
                    todayFirstSec.setHours(0,0,0);
                    todayFirstSec.toLocaleDateString('es-ES',{
                        timeZone: 'America/La_Paz',
                    });
                    console.log(todayFirstSec);

                    var todayString = todayFirstSec.getFullYear() + "-" + (todayFirstSec.getMonth() +1) + "-" + todayFirstSec.getDate();
                    console.log(todayString); 

                    var todayFinalSec = new Date();
                    todayFinalSec.setHours(23, 59, 59);
                    console.log(todayFinalSec);

                    var todayPostsCounter = 0;

                    postsTemp.map((post) => {
                        var parts = post.planned_datetime.split('-');
                        // Please pay attention to the month (parts[1]); JavaScript counts months from 0:
                        // January - 0, February - 1, etc.
                        var plannedDateTime = new Date(parts[0], parts[1] - 1, parts[2]); 
                        plannedDateTime.setHours(0,0,0);
                        plannedDateTime.toLocaleDateString('es-ES',{
                            timeZone: 'America/La_Paz',
                        });
                        //console.error(plannedDateTime);

                        const plannedString = parts[0] + '-' + parts[1] + '-' + parts[2];
                        console.log(plannedString);

                        if(plannedString == todayString){
                            console.warn('here!');
                            todayPostsCounter++;
                            console.error(post.planned_datetime);
                            console.error(plannedDateTime);
                            console.warn(post);
                        } 
                    });

                    setTodayPosts(todayPostsCounter);

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [])


    return (

        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>
            </>
            :

            <div>
                {todayPosts}
            </div>


    )
}