import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import { title } from 'process';
import ModalEditCalendar from './modalEditCalendar';
import ImgVideo from '../../../public/Board/video.png'
import ImgImagen from '../../../public/Board/image.png'




export default function ModalDetalle(props) {
  const {showModal, setShowModal, selectedPost, todaysDate} = props;

    return (
        <>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[350px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">
                                <div>Posts Day</div>
                                <div className='col-span-12 md:col-span-12 lg:col-span-12  bg-[#fff] shadow-md rounded-[20px] p-[20px] mb-[15px]'
                                    key={selectedPost.id}>
                                    <div className="flex flex-row justify-between">
                                        <div className='flex flex-row'>
                                            <div className="w-[48px] h-[48px] rounded-full bg-[#fff] ">
                                                {selectedPost.clients_plan.client.img_url !== null && selectedPost.clients_plan.client.img_url.length > 0 ?
                                                
                                                <Image 
                                                    className='rounded-full'
                                                    src={selectedPost.clients_plan.client.img_url}
                                                    alt='media'
                                                    layout='responsive'
                                                    height={48}
                                                    width={48}
                                                    >

                                                </Image> 
                                                : <></>  
                                                } 
                                
                                            </div>
                                            <div className="pl-[10px] items-center self-center">
                                
                                                {selectedPost.clients_plan.client.name !== null ?
                                                    <p className='text-[14px] font-semibold text-left leading-2 whitespace-normal'>
                                                    {selectedPost.clients_plan.client.name}
                                                    </p>
                                                    : <></>
                                                }
                                                {selectedPost.status !== null ?
                                                    <div className='text-[10px] self-center text-[#582BE7]'>
                                                    {selectedPost.status}
                                                    </div>
                                                    : <></>
                                                }

                                                {/* {
                                                    todaysDate !== null && todaysDate.length > 0 ? 
                                                    <p className='text-[12px]'>{todaysDate}</p>
                                                    : <></>
                                                } */}

                                                

                                            
                                            </div>
                                        </div>

                                        <div className='text-[12px]'>
                                            {selectedPost.type === 'Video' ?
                                                <Image 
                                                    
                                                    src={ImgVideo}
                                                    alt='media'
                                                    layout='fixed'
                                                    height={48}
                                                    width={48}
                                                    >

                                                </Image> 
                                            : <></>  
                                            } 
                                            {selectedPost.type === 'Image' ?
                                                <Image 
                                                    
                                                    src={ImgImagen}
                                                    alt='media'
                                                    layout='fixed'
                                                    height={48}
                                                    width={48}
                                                    >

                                                </Image> 
                                            : <></>  
                                            } 
                                            {selectedPost.type === 'Album' ?
                                                <Image 
                                                    
                                                    src={ImgImagen}
                                                    alt='media'
                                                    layout='fixed'
                                                    height={48}
                                                    width={48}
                                                    >

                                                </Image> 
                                            : <></>  
                                            }
                                            {selectedPost.type === 'Story' ?
                                                <Image 
                                                    
                                                    src={ImgVideo}
                                                    alt='media'
                                                    layout='fixed'
                                                    height={48}
                                                    width={48}
                                                    >

                                                </Image> 
                                            : <></>  
                                            }
                                            {selectedPost.type === 'Reel' ?
                                                <Image 
                                                    
                                                    src={ImgVideo}
                                                    alt='media'
                                                    layout='fixed'
                                                    height={48}
                                                    width={48}
                                                    >

                                                </Image> 
                                            : <></>  
                                            }
                                          
                                        </div>
                            
                                    </div>
                                    <div className='mt-[15px] items-center text-center'>
                                        {selectedPost.media_url != null && selectedPost.media_url.length > 0 ?
                        
                                                <Image
                                                className=''
                                                src={selectedPost.media_url}
                                                alt='media'
                                                layout='responsive'
                                                height={260}
                                                width={260}
                                                >
                                            </Image>
                                                : <></>  
                                            }
                                    </div>
                                    {selectedPost.post_copy !== null && selectedPost.post_copy !== undefined ?
                                    <div className='mt-[5px] mb-[5px]'>
                                        <p className='text-[12px] leading-4 whitespace-normal'>{selectedPost.post_copy}</p>
                                        
                                    </div>
                                    : <></>
                                    }
                                    <hr></hr>
                                    <div className='mt-[5px]'>
                                        {selectedPost.title !== null ?
                                        <p className='text-[12px] font-semibold text-left leading-2 whitespace-normal text-ellipsis ...'>
                                        {selectedPost.title}
                                        </p>
                                        : <></>
                                        }
                                        {selectedPost.suttitle !== null ?
                                            <p className='text-[10px] text-left leading-2 whitespace-normal text-ellipsis ...'>
                                            {selectedPost.subtitle}
                                            </p>
                                            : <></>
                                        }
                                        {selectedPost.instructions !== null && selectedPost.instructions !== undefined ?
                                        <div className='mt-[5px] mb-[5px]'>
                                            <div className='font-semibold text-[12px] text-[#582BE7]'>Instructions: </div>
                                            <p className='text-[12px] font-light leading-4 whitespace-normal'>{selectedPost.instructions}</p>
                                        </div>
                                        : <></>
                                        }
                                    </div>
                                    <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[15px]'> 
                                    <ModalEditCalendar
                                        id={selectedPost.id}
                                        title={selectedPost.title}
                                        subtitle={selectedPost.subtitle}
                                        post_copy={selectedPost.post_copy}
                                        social_network={selectedPost.social_network}
                                        planned_datetime={selectedPost.planned_datetime}
                                        type={selectedPost.type}
                                        instructions={selectedPost.instructions}
                                    />
                                        
                                    </div>
                        
                                </div>

                                <div className='text-end'>
                                    <button
                                        className="w-[100px] h-[35px] border-[1px] border-[#582BE7] rounded-[30px] text-[#582BE7] hover:bg-[#582BE7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                        onClick={() =>
                                            setShowModal(false)
                                        }
                                    >
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}
